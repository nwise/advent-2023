# frozen_string_literal: true

require './init.rb'

input = [ "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n",
      "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n",
      "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n",
      "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n",
      "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green" ].join
input = File.readlines('./data/day2.txt').join

LIMITS = {
  red: 12,
  green: 13,
  blue: 14
}.freeze

@valid = Set.new

def games(input)
  input.split("\n").each { _1 }
end

def rounds(game)
  game.gsub(/Game \d+:/, '').split(';')
end

def draws(round)
  round.split(',') 
end

def valid?(draw)
  LIMITS[draw.keys.first.to_sym] >= draw.values.first.to_i
rescue StandardError => e
  binding.pry
end

def color_counts(games)
  counts = {}
  x = games.map do |game|
    values = [] 
    game_id = game.scan(/Game \d+:/).first.scan(/\d+/).first.to_i
    rounds = rounds(game)
    counts[game_id] ||= {} 
    max = { blue: 0, green: 0, red: 0 }
    rounds.each.with_index(1) do |round, round_id|
      counts[game_id][round_id] ||= []
      draws = draws(round)
      draws.each do |draw|
        count, color = draw.split(' ')
        color = color.to_sym
        count = count.to_i
        d = { color => count }
        counts[game_id][round_id] << d
        max[color] = [max[color], d[color]].max  
      end
    end
    value = max.values.reduce(:*)
    puts "#{game_id}: #{value}"
    value
  end.sum
  puts x
  counts
end

# game_list = games(input)
# games = color_counts(game_list)

# games.each do |game_id, rounds|
#   valid = rounds.all? do |_round_id, draw|
#     draw.all? { valid?(_1) }
#   end
#   @valid.add(game_id) if valid
# end

# puts "Part 1: #{@valid.sum}"

game_list = games(input)
games = color_counts(game_list)



binding.pry