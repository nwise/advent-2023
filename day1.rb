# frozen_string_literal: true

require './init.rb'

input = %w[1abc2 pqr3stu8vwx a1b2c3d4e5f treb7uchet]
input = File.readlines('./data/day1.txt')

def get_code(input, matcher)
  input.map do |row|
    digits = row.scan(matcher)
    digits = convert_digits(digits)
    first = digits.first
    last = digits.last
    x = "#{first}#{last}".to_i
    puts x
    x
  end.sum
end

def convert_digits(digits)
  digits.map do |digit|
    case digit
    when 'one'
      1
    when 'two'
      2
    when 'three'
      3
    when 'four'
      4
    when 'five'
      5
    when 'six'
      6
    when 'seven'
      7
    when 'eight'
      8
    when 'nine'
      9
    else
      digit
    end
  end
end

  # puts "Result 1: #{get_code(input, /\d/)}"

  #input = %w[two1nine eightwothree abcone2threexyz xtwone3four 4nineeightseven2 zoneight234 7pqrstsixteen]
  pattern = /\d|one|two|three|four|five|six|seven|eight|nine/

  puts "Result 2: #{get_code(input, pattern)}"
